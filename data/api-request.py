# Python program to scrape Wikipedia

# Ten prosty skrypt zczytuje informacje z danej strony i wypisuje je na konsoli

import requests
import wikipedia
from bs4 import BeautifulSoup
import csv
from urllib.request import urlopen

wikipediaPage = wikipedia.WikipediaPage(title = 'Death_of_Muammar_Gaddafi')

# Pobranie streszczenia danego artykułu
# print(wikipedia.summary(wikipediaPage))

# Cała treść strony
print(wikipediaPage.content)

# Cała strona w html
pageInHTML = wikipediaPage.html()

# Kategorie strony
print(wikipediaPage.categories)

# Lista wszystkich zdjęć na stronie
listOfPageImages = wikipediaPage.images

# Lista wszystkich linków na stronie
listOfLinks = wikipediaPage.links

# Lista wszystkich ODNOŚNIKÓW (linków do innych wpisów) na stronie
listOfReferences = wikipediaPage.references

# Wszystkie języki na stronie
listOfLanguage = wikipedia.languages()